has_protein = lambda foodItem: "proteins" in foodItem["nutrition"]
has_kj = lambda foodItem: "kilojoules" in foodItem["nutrition"]
has_fat = lambda foodItem: "fat" in foodItem["nutrition"]
has_carbs = lambda foodItem: "carbohydrates" in foodItem["nutrition"]
