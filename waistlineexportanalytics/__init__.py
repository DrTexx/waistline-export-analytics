# NOTE: (accurate as of 2023/09/27)
# ... On Android (without a VM), Poetry installation requires the cryptography
# ... Python package. The Cryptography Python package requires the Rust compiler
# ... to be installed in order to compile Cryptography on Android (without a VM).
# ... (i.e. if you want to use Poetry on your Android (without a VM), you're going
# ... to need to install the Rust compiler, which is probably possible but is also
# ... probably not worth the effort required)

# --- imports ---

# built in
import os
import json
import textwrap as tw
from pprint import pprint
from pathlib import Path
# site
from colorama import Fore, Back, Style, init as init_colorama
from tabulate import tabulate
# package
from .fooditem import has_protein, has_kj, has_fat, has_carbs

# --- constants ---

# waistline export analytics settings file filepath
WEA_SETTINGS_FILE_FILEPATH = './wea-settings.json'

# NOTE: used to fix table spacing when emojis are used in tables when emulating Ubuntu on Android
# zero-width joiner char inside this string
ZWJ = "‍"

# --- misc setup ---

init_colorama()

if not Path(WEA_SETTINGS_FILE_FILEPATH).exists():
	# TODO: suggest user maybe creates the file and provide link to template that's in the project's repo
	raise FileNotFoundError("Failed to find your `wea-settings.json` file, perhaps you haven't created it yet or you're in the wrong directory?")
	
with open(WEA_SETTINGS_FILE_FILEPATH, 'r') as f:
	wea_settings = json.load(f)
	if not ("schemaVersion" in wea_settings):
		raise Exception("`wea_settings.json` is missing a `schemaVersion` key")
	if wea_settings["schemaVersion"] == "1":
		waistline_export_dirpath = wea_settings["pathToDirContainingWaistlineExport"]
		daily_protein_goal = wea_settings["dailyProteinGoal"]
	else:
		if not isinstance(wea_settings["schemaVersion"],str):
			raise Exception("schemaVersion must be a string")
		else:
			raise Exception("Unsupported wea_settings.json schemaVersion")

print(os.getcwd())
print(os.listdir(waistline_export_dirpath))

# --- functions and lambdas ---

def print_food_item(foodItem):
	#pprint(foodItem)
	_has_proteins = has_proteins(foodItem)
	_has_kj = has_kj(foodItem)
	print(
		foodItem["name"],
		"\n 🦄 ",
		foodItem.get("brand", "[UNKNOWN]"),
		"\n (per serve) 💪",
		foodItem["nutrition"]["proteins"] if _has_proteins else "N/A",
		"🔥",
		foodItem["nutrition"]["kilojoules"] if _has_kj else "N/A",
		#"\n 💪 (per 100) ",
		#(
		#	foodItem["nutrition"]["proteins"]
		#	/ float(foodItem["portion"])
		#)
		#* 100 if _has_proteins else "N/A",
		#"\n 💪 per 🔥",
		#(
		#	foodItem["nutrition"]["proteins"]
		#	/ foodItem["nutrition"]["kilojoules"]
		#) if _has_proteins and _has_kj else "N/A",
		"\n 💪 % per 🔥 ",
		round((
			foodItem["nutrition"]["proteins"]
			/ foodItem["nutrition"]["kilojoules"]
		) * 100, 2) if _has_proteins and _has_kj else "N/A",
		"% (rounded)"
	)

def list_food_items(data):
	for foodItem in data["foodList"][:2]:
		print_food_item(foodItem)
	print("it worked!")
	
def get_protein_per_100(foodItem):
	#print([key for key in foodItem["nutrition"]])
	if not "proteins" in foodItem["nutrition"]:
		print("food item '" + foodItem["name"] + "' has no protein info recorded :(")
		return -1	
	
	return (
		(
			foodItem["nutrition"]["proteins"]
			/ float(foodItem["portion"])
		)
		* 100
	)
	
def get_protein_per_kj(foodItem):
	#print([key for key in foodItem["nutrition"]])
	if not "proteins" in foodItem["nutrition"]:
		print("food item '" + foodItem["name"] + "' has no protein info recorded :(")
		return -1
	if not "kilojoules" in foodItem["nutrition"]:
		print("food item '" + foodItem["name"] + "' has no kilojoules info recorded :(")
		return -1
	
	return (
		foodItem["nutrition"]["proteins"]
		/ foodItem["nutrition"]["kilojoules"]
	)
	
def list_by_protein_per_100(data):
	data_sorted = sorted(data["foodList"],key=get_protein_per_100)
	for foodItem in data_sorted:
		print_food_item(foodItem)
		
def list_by_protein_per_kj(data):
	data_sorted = sorted(data["foodList"],key=get_protein_per_kj)
	for foodItem in data_sorted:
		# note only print food items with proteins recorded
		if "proteins" in foodItem["nutrition"]:
			# ignore stuff with 0g or less of protein
			if foodItem["nutrition"]["proteins"] > 0:
				# ignore stuff with less than 1g of protein
				if foodItem["nutrition"]["proteins"] >= 1:
					print_food_item(foodItem)
			
def apply_filters(iterable, filters):
	result = iterable
	for _filter in filters:
		result = filter(_filter, result)
	return result

def protein_atleast_g_per_serve(min_grams):
	return lambda foodItem: float(foodItem["nutrition"]["proteins"]) >= min_grams
	
def create_custom_food_item_formatter(
	schema_version,
	conditional_formatting
):
	def custom_fif(foodItem):
		#TODO
		name = foodItem["name"]
		brand = foodItem.get("brand", "[UNKNOWN]")
		kilojoules = foodItem["nutrition"].get("kilojoules", None)
		proteins = foodItem["nutrition"].get("proteins", None)
		proteins_percent_per_kj = round((
			proteins
			/ kilojoules
		) * 100, 2)
		
		return f"🏷️ {name}\n  🦄 {brand}\n  🔥 {kilojoules} 💪 {proteins}\n  proteins % per kj - [{proteins_percent_per_kj}]"
		
		#foodItem["name"],
#		"\n 🦄 ",
#		foodItem.get("brand", "[UNKNOWN]"),
#		"\n (per serve) 💪",
#		foodItem["nutrition"]["proteins"] if _has_proteins else "N/A",
#		"🔥",
#		foodItem["nutrition"]["kilojoules"] if _has_kj else "N/A",
	return custom_fif
	
def kj_less_than(max_kj):
	return lambda foodItem: foodItem["nutrition"]["kilojoules"] < max_kj

def print_protein_per_kj_table(data):
	
	# apply filters
	foodList_filtered = apply_filters(
		data["foodList"],
		[
			has_protein,
			has_kj,
			protein_atleast_g_per_serve(0.05),
			#protein_atleast_g_per_serve(0.5),
			
			#FIXME: TEMP FILTERS
			#lambda foodItem: foodItem.get("brand",None) == "McVities"
			#lambda foodItem: foodItem.get("brand",None) == "Hungry Jacks",
			#lambda foodItem: foodItem["nutrition"].get("sugars",0) >= 5
			#protein_atleast_g_per_serve(18),
			#kj_less_than(1500)
		]
	)
	
	foodList_sorted = sorted(
		foodList_filtered,
		key=get_protein_per_kj,
		#key=get_keto_rating(70,30,10)
	)
	
	_custom_fif = create_custom_food_item_formatter(
		schema_version="1.0",
		conditional_formatting={
			"protein": [
				protein_atleast_g_per_serve(10),
				lambda text: Fore.GREEN + text + Style.RESET_ALL
			]
		}
	)
	
	#for foodItem in foodList_sorted:
		#print_food_item(foodItem)
		#print(_custom_fif(foodItem))
	
	table_data = [["name", "brand"]]
	for foodItem in foodList_sorted:
		name = foodItem["name"]
		brand = foodItem.get("brand", "[UNKNOWN]")
		portion = foodItem["portion"]
		unit = foodItem["unit"]
		kilojoules = foodItem["nutrition"].get("kilojoules", None)
		proteins = foodItem["nutrition"].get("proteins", None)
		sugars = foodItem["nutrition"].get("sugars", None)
		proteins_percent_per_kj = (
			round(
				(proteins / kilojoules)
				* 100,
				2
			)
		)
	
		kj_required_to_reach_entire_daily_protein_goal = (daily_protein_goal / proteins) * kilojoules
		serving_required_to_reach_entire_daily_protein_goal = (daily_protein_goal / proteins) * float(portion)
		
		# TODO: move to constants
		# max width of col 2
		MWC2 = 17
		
		table_data.append([
			tw.fill(name, 15),
			tw.fill(brand, 9),
			tw.fill(
				ZWJ
				+ "📦 "
				+ str(portion)
				+ unit,
				MWC2
			)
			+ "\n"
			+ tw.fill(
				ZWJ
				+ "🔥 "
				+ (Fore.GREEN if kilojoules <= 500 else "")
				+ str(kilojoules)
				+ Style.RESET_ALL,
				MWC2
			)
			+ "\n"
			+ tw.fill(
				ZWJ
				+ "💪 "
				+ (Fore.GREEN if proteins >= 10 else "")
				+ str(proteins)
				+ Style.RESET_ALL,
				MWC2
			)
			+ "\n"
			+ tw.fill(
				ZWJ
				+ "🍪 "
				#+ (Fore.GREEN if sugars <= 5 else "")
				+ str(sugars),
				#+ Style.RESET_ALL
				MWC2
			)
			+ "\n"
			+ tw.fill(
				ZWJ
				+ "🔷 "
				+ str(proteins_percent_per_kj),
				MWC2
			)
			+ "\n"
			# how many kj would be necessary if my entire protein intake for the day came from just this food (e.g. I'd need to have 2401.63kj of beef jerky to get 90g of protein from it - if 90g of protein were my daily protein intake goal that is)
			+ tw.fill(
				ZWJ
				+ "❔ "
				+ str(round(kj_required_to_reach_entire_daily_protein_goal,2))
				+ "kj",
				MWC2
			)
			+ "\n"
			+ tw.fill(
				ZWJ
				+ "❓ "
				+ str(round(serving_required_to_reach_entire_daily_protein_goal,2))
				+ unit,
				MWC2
			)
		])

	print(tabulate(
		table_data,
		headers="firstrow",
		tablefmt="fancy_grid",
		#tablefmt="grid",
		#maxcolwidths=[15,None]
	))
	
	print(
		"📦 = serving size",
		"\n🔥 = kilojoules (per serve)",
		"\n💪 = protein (per serve)",
		"\n🍪 = sugar (per serve)",
		"\n🔷 = protein % per kilojoule",
		"\n❔ = minimum kj required to reach entire daily protein goal if I ate nothing but this",
		"\n❓ = minimum serving required to reach entire daily protein goal if I ate nothing but this"
	)
	print()
	
def get_percent_delta_score(real_percent,target_percent):
	return 1 - (
		abs(
			real_percent
			- target_percent
		)
		/100
	)

def get_keto_rating(
	fat_percent_goal,
	proteins_percent_goal,
	carbs_percent_goal
):
	def _calc_keto_rating(foodItem):
		# get fat/proteins/carbs ratio
		fpc_percents = get_fpc_percents(foodItem)
  
		# if None returned (fpc sum was 0)
		if fpc_percents is None:
			# TODO: reflect on if this is actually the behavior I want
			# return perfect score?
			return 1
		else:
			fat_percent, proteins_percent, carbs_percent = fpc_percents
		
		# get percentage deltas
		fat_score = get_percent_delta_score(
			fat_percent,
			fat_percent_goal
		)
		proteins_score = get_percent_delta_score(
			proteins_percent,
			proteins_percent_goal
		)
		carbs_score = get_percent_delta_score(
			carbs_percent,
			carbs_percent_goal
		)
		
		# calculate derived keto rating
		keto_score = (
			fat_score
			+ proteins_score
			+ carbs_score
		) / 3
		
		return keto_score
		
	return _calc_keto_rating

def get_fpc_percents(foodItem):
	# get essential information
	fat = float(foodItem["nutrition"]["fat"])
	proteins = float(foodItem["nutrition"]["proteins"])
	carbs = float(foodItem["nutrition"]["carbohydrates"])
	
	# combine relevant macro grams in order to get percentages
	gram_sum = fat + proteins + carbs
	
	# if macro gram sum is 0 or less
	if gram_sum <= 0:
		print("GRAM SUM 0 OR LESS!:", foodItem["name"], foodItem["brand"])
		return None
	
	# get percentages
	fat_percent = (fat / gram_sum)*100
	proteins_percent = (proteins / gram_sum)*100
	carbs_percent = (carbs / gram_sum)*100
	
	return [
		fat_percent,
		proteins_percent,
		carbs_percent
	]

def print_keto_rating_table(data):
		# apply filters
		foodList_filtered = apply_filters(
			data["foodList"],
			[
				has_fat,
				has_protein,
				has_carbs
			]
		)
		
		# generate func to get scores based on SKD (Standard Ketogenic Diet) ratios
		get_skd_rating = get_keto_rating(70,30,10)
		
		# calculate keto ratings
#		foodList_with_keto_ratings = map(
#			lambda foodItem:
#				foodItem.update(
#					{
#						"keto_rating":
#							get_skd_rating(foodItem)
#					}
#				),
#			foodList_filtered
#		)
		
		# TODO: find way to avoid calculating keto rating more than once (here and when displaying in table)
		# apply sorting
		foodList_sorted = sorted(
			foodList_filtered,
			key=get_skd_rating
		)
		
		# build table data
		table_data = [
			["name","brand","details"]
		]
		for foodItem in foodList_sorted:
			fpc_percents = get_fpc_percents(foodItem)
   
			if fpc_percents is None:
				fat_percent, proteins_percent, carbs_percent = ["N/A","N/A","N/A"]
			else:
				# TODO: find another way to do this so rounded figures aren't accidentally used instead of original ones if there are any calculations made beyond this point
				# round fpc percents to 2 decimal places
				fpc_percents = [round(percent,2) for percent in fpc_percents]

				fat_percent, proteins_percent, carbs_percent = fpc_percents
	
			# TODO: move to constants
			WRAP_CHAR_LIMIT = 19
   
			table_data.append([
				foodItem.get("name","[ UNKNOWN ]"),
				foodItem.get("brand","[ UNKNOWN ]"),
				tw.fill(
					"score: "
					+ str(round(get_skd_rating(foodItem),2)),
					WRAP_CHAR_LIMIT
				)
				+ "\n"
				+ tw.fill(
					"fat(%): "
					+ str(fat_percent),
					WRAP_CHAR_LIMIT
				)
				+ "\n"
				+ tw.fill(
					"proteins(%): "
					+ str(proteins_percent),
					WRAP_CHAR_LIMIT
				)
				+ "\n"
				+ tw.fill(
					"carbs(%): "
					+ str(carbs_percent),
					WRAP_CHAR_LIMIT
				)
			])

		# print table
		print(tabulate(
			table_data,
			headers="firstrow",
			tablefmt="fancy_grid",
			#tablefmt="grid",
			maxcolwidths=[10,8,None]
		))
  
# --- functions for calling externally (basically scripts) ---

def main_protein():
	with open(Path(waistline_export_dirpath).joinpath("waistline_export.json"), "r") as f:
		data = json.load(f)
		#print([key for key in data])
		#list_food_items(data)
		#list_by_protein_per_100(data)
		#list_by_protein_per_kj(data)
		print_protein_per_kj_table(data)

def main_keto():
	with open(Path(waistline_export_dirpath).joinpath("waistline_export.json"), "r") as f:
		data = json.load(f)
		print_keto_rating_table(data)
