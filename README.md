# Waistline Export Analytics

Gain valuable health and dieting insights using database backups from the app [Waistline](https://github.com/davidhealey/waistline).

## Background
WEA is a side project of mine that I've developed to help me with my day-to-day weight and calorie tracking. For convenience sake, I tend to run it inside of an Ubuntu VM on my Android device as I want to process of re-exporting my data from Waistline and then processing it to be as fast as possible (I'd rather not sync my waistline_export.json to my computer every time I want to figure out things like which newly added foods in my db have the best protein to kj ratios).

**Note:** I am not the developer of the Waistline app.

## Getting started

TODO

## TODO

- [ ] Set up CI/CD
- [ ] Add README badges
- [ ] List features
- [ ] Usage information
- [ ] List any similar projects and the differentiating factors between WEA and them
- [ ] Include screenshots/GIFs of project (try ttygif or Asciinema)

<!--
## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->
